# Apimage

The purpose of this project is to extract metadata from pictures.

## Technology

* Java 8
* Spring Boot
* Spring

## Changelog

**1.0.0 (final realease):**
Adding GPS converter for Gmaps integration and cleaning the program code.

**0.9.0:**
The whole application works except gmaps integration.

**0.0.2:**
Controllers concerning the customer and admin are developped and stable.

**0.0.1:**
Initial release.