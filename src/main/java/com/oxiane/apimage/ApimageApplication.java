package com.oxiane.apimage;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApimageApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApimageApplication.class, args);
	}

}
