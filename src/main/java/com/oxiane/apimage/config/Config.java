package com.oxiane.apimage.config;

import javax.sql.DataSource;

import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class Config {
	@Bean
    public DataSource getDataSource() {
        return DataSourceBuilder.create()
        		.driverClassName("org.h2.Driver")
        		.url("jdbc:h2:mem:test")
        		.username("admin")
        		.password("admin")
        		.build();
    }
}
