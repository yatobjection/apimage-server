package com.oxiane.apimage.extractmetadata;

import java.io.File;
import java.io.IOException;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.drew.imaging.ImageMetadataReader;
import com.drew.imaging.ImageProcessingException;
import com.drew.metadata.Directory;
import com.drew.metadata.Metadata;
import com.drew.metadata.Tag;

@RestController
public class ExtractMetadata {

	File jpegfile = new File("C:\\Users\\khuongduy\\Oxiane\\eclipse-workspace\\apimage\\src\\main\\resources\\Bibi.jpg");
	
	@RequestMapping("/")
	public void display() {
		Metadata metadata = null;
		try {
			metadata = ImageMetadataReader.readMetadata(jpegfile);
		} catch (ImageProcessingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		for (Directory directory : metadata.getDirectories()) {
		    for (Tag tag : directory.getTags()) {
		    	//String tagName = tag.getTagName();
		        System.out.println(tag);
		    }
		}
	}
}
